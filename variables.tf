variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "server_name" {}
variable "ec2_ami_id" {}
variable "ec2_instance_type" {}
variable "ec2_region" {}
variable "ec2_availability_zone" {}

variable "ec2_instance_count" {}
variable "ec2_associate_public_ip_address" {}

variable "ec2_instance_user" {}
variable "ec2_private_key_name" {}
variable "ec2_private_key_path" {}

variable "ebs_block_device_name" {}
variable "ebs_volume_type" {}
variable "ebs_volume_size" {}
#variable "ebs_snapshot_id" {}

variable "docker_source" {}
variable "compose_files_source" {}

variable "rds_instance_identifier" {
  description = "Custom name of the instance"
}

variable "rds_storage_type" {
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD)."
  default = "standard"
}

variable "rds_storage_encrypted" {}

variable "rds_allocated_storage" {
  description = "The allocated storage in GBs"
  # You just give it the number, e.g. 10
}

variable "rds_engine_type" {
  description = "Database engine type"
  # Valid types are
  # - mysql
  # - postgres
  # - oracle-*
  # - sqlserver-*
  # See http://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html
  # --engine
}

variable "rds_engine_version" {
  description = "Database engine version, depends on engine type"
  # For valid engine versions, see:
  # See http://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html
  # --engine-version

}

variable "rds_instance_class" {
  description = "Class of RDS instance"
  # Valid values
  # https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html
}

variable "rds_db_name" {
  description = "The name of the database to create"
}

# Self-explainatory variables
variable "rds_db_user" {}
variable "rds_db_password" {}
variable "rds_db_port" {}


variable "rds_is_multi_az" {
  description = "Set to true on production"
  default = false
}

variable "rds_availability_zone" {}

variable "rds_auto_minor_version_upgrade" {
  description = "Allow automated minor version upgrade"
  default = true
}

variable "rds_allow_major_version_upgrade" {
  description = "Allow major version upgrade"
  default = false
}

# This is for a custom parameter to be passed to the DB
# We're "cloning" default ones, but we need to specify which should be copied
variable "rds_db_parameter_group" {
  description = "Parameter group, depends on DB engine used"
  # default = "mariadb10.0"
  # default = "mysql5.6"
  # default = "postgres9.5"
}

variable "rds_publicly_accessible" {
  description = "Determines if database can be publicly available (NOT recommended)"
  default = false
}

variable "rds_skip_final_snapshot" {
  description = "If true (default), no snapshot will be made before deleting DB"
  default = true
}

variable "rds_final_snapshot_identifier" {}

variable "rds_copy_tags_to_snapshot" {
  description = "Copy tags from DB to a snapshot"
  default = true
}

variable "rds_backup_window" {
  description = "When AWS can run snapshot, can't overlap with maintenance window"
  default = "22:00-03:00"
}

variable "rds_backup_retention_period" {
  type = "string"
  description = "How long will we retain backups"
  default = 0
}

# RDS Subnet Group Variables
#variable "subnets" {
#  description = "List of subnets DB should be available at. It might be one subnet."
#  type = "list"
#}

variable "default_vpc_id" {}
variable "default_sg_id" {}
variable "default_subnet_id" {}

variable "subnet_1" {}
variable "subnet_2" {}

variable "private_cidr" {
  description = "VPC private addressing, used for a security group"
  type = "string"
}

variable "vpc_id" {
  description = "VPC to connect to, used for a security group"
  type = "string"
}




