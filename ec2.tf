resource "aws_eip" "docker" {
  instance = "${aws_instance.docker.id}"
  vpc = true

  provisioner "local-exec" {
    command = "rm ~/.ssh/config && echo Host ${var.server_name} >> ~/.ssh/config && echo User ${var.ec2_instance_user} >> ~/.ssh/config && echo HostName ${aws_eip.docker.public_ip} >> ~/.ssh/config && echo IdentityFile ${var.ec2_private_key_path} >> ~/.ssh/config && echo Port 22 >> ~/.ssh/config"
  }
}

resource "aws_instance" "docker" {

  instance_type = "${var.ec2_instance_type}"
  ami = "${var.ec2_ami_id}"
  key_name = "${var.ec2_private_key_name}"
  subnet_id = "${var.default_subnet_id}"
  security_groups = ["${var.default_sg_id}"]
  availability_zone = "${var.ec2_availability_zone}"
  count = "${var.ec2_instance_count}"
  associate_public_ip_address = "${var.ec2_associate_public_ip_address}"

  tags {
     Name = "${var.server_name}"
  }

  ebs_block_device {
    device_name = "${var.ebs_block_device_name}"
    volume_type = "${var.ebs_volume_type}"
    volume_size = "${var.ebs_volume_size}"
    delete_on_termination = true
  }

  connection {
    type = "ssh"
    user = "${var.ec2_instance_user}"
    host = "${aws_instance.docker.public_ip}"
    port = 22
    timeout = "3m"
    agent = false
    private_key = "${file(var.ec2_private_key_path)}"
  }

  provisioner "local-exec" {
    command = "chmod 600 ${var.ec2_private_key_path}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /opt",
      "sudo chmod -R 777 /opt"
    ]
  }

  #provisioner "file" {
  #  source = "${var.docker_source}"
  #  destination = "/opt"
  #}

  #provisioner "file" {
  #  source = "${var.compose_files_source}"
  #  destination = "/opt"
  #}

  provisioner "file" {
    source = "provision/ansible.sh"
    destination = "/opt/ansible.sh"
  }

  provisioner "file" {
      source = "provision/Makefile"
      destination = "/opt/Makefile"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /opt/ansible.sh",
      "chmod +x /opt/Makefile",
      "cd /opt && bash ansible.sh",
      #"cd /opt/docker && make docker",
      "sudo apt-get update",
      "sudo apt-get dist-upgrade -y",
      "sudo reboot"
    ]
  }
}