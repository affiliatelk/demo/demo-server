//
// Module: tf_aws_rds
//

// This template creates the following resources
// - An RDS instance
// - A database subnet group
// - You should want your RDS instance in a VPC

resource "aws_db_instance" "main_rds_instance" {
  identifier = "${var.rds_instance_identifier}"
  allocated_storage = "${var.rds_allocated_storage}"
  engine = "${var.rds_engine_type}"
  engine_version = "${var.rds_engine_version}"
  instance_class = "${var.rds_instance_class}"

  name = "${var.rds_db_name}"
  username = "${var.rds_db_user}"
  password = "${var.rds_db_password}"
  port = "${var.rds_db_port}"

  # Because we're assuming a VPC, we use this option, but only one SG id
  vpc_security_group_ids = ["${aws_security_group.main_db_access.id}"]

  # We're creating a subnet group in the module and passing in the name
  db_subnet_group_name = "${aws_db_subnet_group.main_db_subnet_group.name}"
  parameter_group_name = "${aws_db_parameter_group.main_rds_instance.id}"

  # We want the multi-az setting to be toggleable, but off by default
  multi_az = "${var.rds_is_multi_az}"
  availability_zone = "${var.rds_availability_zone}"
  storage_type = "${var.rds_storage_type}"
  storage_encrypted = "${var.rds_storage_encrypted}"
  publicly_accessible = "${var.rds_publicly_accessible}"

  # Upgrades
  allow_major_version_upgrade = "${var.rds_allow_major_version_upgrade}"
  auto_minor_version_upgrade  = "${var.rds_auto_minor_version_upgrade}"

  # Snapshots and backups
  skip_final_snapshot = "${var.rds_skip_final_snapshot}"
  final_snapshot_identifier = "${var.rds_final_snapshot_identifier}"
  copy_tags_to_snapshot = "${var.rds_copy_tags_to_snapshot}"

  backup_retention_period = "${var.rds_backup_retention_period}"
  backup_window = "${var.rds_backup_window}"

}

resource "aws_db_parameter_group" "main_rds_instance" {
  name = "${var.rds_instance_identifier}-${replace(var.rds_db_parameter_group, ".", "")}-custom-params"
  family = "${var.rds_db_parameter_group}"

#   parameter {
#     name = "character_set_server"
#     value = "utf8"
#   }

#   parameter {
#     name = "character_set_client"
#     value = "utf8"
#   }
}

resource "aws_db_subnet_group" "main_db_subnet_group" {
  name = "${var.rds_instance_identifier}-subnetgrp"
  description = "RDS subnet group"
  subnet_ids = ["${var.subnet_1}", "${var.subnet_2}"]
}

# Security groups
resource "aws_security_group" "main_db_access" {
  name = "${var.rds_instance_identifier}-access"
  description = "Allow access to the database"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_db_access" {
  type = "ingress"

  from_port = "${var.rds_db_port}"
  to_port = "${var.rds_db_port}"
  protocol = "tcp"
  cidr_blocks = ["${var.private_cidr}"]

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_all_outbound" {
  type = "egress"

  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.main_db_access.id}"
}

# Configure the MySQL provider based on the outcome of creating the aws_db_instance.
#provider "mysql" {
#  endpoint = "${aws_db_instance.main_rds_instance.endpoint}"
#  username = "${aws_db_instance.main_rds_instance.username}"
#  password = "${aws_db_instance.main_rds_instance.password}"
#}

# Create a second database, in addition to the "initial_db" created by the aws_db_instance resource above.
#resource "mysql_database" "magento" {
#  name = "magento"
#}

#resource "mysql_database" "prestashop" {
#  name = "prestashop"
#}

#resource "mysql_database" "opencart" {
#  name = "opencart"
#}